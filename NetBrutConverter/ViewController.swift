//
//  ViewController.swift
//  NetBrutConverter
//
//  Created by Sema Dogan on 08/07/2020.
//  Copyright © 2020 Suleyman DOGAN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var grossDayTextField: UITextField!
    @IBOutlet weak var grossMonthTextField: UITextField!
    @IBOutlet weak var grossYearTextField: UITextField!
    
    @IBOutlet weak var netDayTextField: UITextField!
    @IBOutlet weak var netMonthTextField: UITextField!
    @IBOutlet weak var netYearTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onGrossYearTextFieldChanged(_ sender: Any) {
        let val: String? = grossYearTextField.text
        
        if val != nil {
            let netVal = Double(val!)
            if netVal != nil {
                netYearTextField.text = "\(netVal! * 1.28)"
            }
        }
    }
    
}

